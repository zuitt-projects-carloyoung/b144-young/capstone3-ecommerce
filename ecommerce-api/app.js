const express = require("express");
const mongoose = require('mongoose');
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const expressValidator = require('express-validator')
require("dotenv").config();
// import routes
const authRoutes = require('./routes/auth') 
const userRoutes = require('./routes/user') 
const categoryRoutes = require('./routes/category') 
const productRoutes = require('./routes/product') 
const braintreeRoutes = require('./routes/braintree') 
const orderRoutes = require('./routes/order') 


// app
const app = express();

// load env variables
const dotenv = require('dotenv');
dotenv.config()
 
//db connection
// mongoose.connect(
//   process.env.DATABASE,
//   {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
//   })
// .then(() => console.log('DB Connected'))

// middlewares
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(cookieParser())
app.use(expressValidator());
app.use(cors());



 // routes middleware
 app.use("/api", authRoutes); 
 app.use("/api", userRoutes); 
 app.use("/api", categoryRoutes); 
 app.use("/api", productRoutes); 
 app.use("/api", braintreeRoutes); 
 app.use("/api", orderRoutes); 
 
// mongoose.connection.on('error', err => {
//   console.log(`DB connection error: ${err.message}`)
// });

const port = process.env.PORT || 8000;

mongoose.connect("mongodb+srv://dbUser:dbUser@wdc028-course-booking.0pav9.mongodb.net/udemy?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology:true
})

mongoose.connection.once('open',() => console.log('Now Connected to MongoDB Atlas'))
app.listen(process.env.PORT || port, () => console.log(`Now listening to port ${process.env.PORT || port}`))




// app.listen(port, () => {
//   console.log(`Server is running on port ${port}`);
// })
//  